export class Model {
    category: string;
    title: string;
    date: Date;
    author: string;
    text: string;
    _id: string;
    type: string;
    image: string;
    status: boolean;
    keywords: string;

    constructor( category, title, date, author, text, _id, type, image, keywords ) {
        this.category = category;
        this.title = title;
        this.date = date;
        this.author = author;
        this.text = text;
        this._id = _id;
        this.type = type;
        this.image = image;
        this.keywords = keywords;
    }
}
