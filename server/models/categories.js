const mongoose = require('mongoose');

let CategorySchema = new mongoose.Schema({
    help: String,
    news: String,
    posts: String
},{
    collection: 'categories'
});

const Category = mongoose.model('Category', CategorySchema)

module.exports = Category