import { Component, OnInit } from '@angular/core';
import { FirebaseService } from '../firebase.service';
import { ActivatedRoute } from "@angular/router";
import { TouchSequence } from 'selenium-webdriver';
import { CategoryListService } from '../category-list.service';

@Component({
  selector: 'app-post-page',
  templateUrl: './post-page.component.html',
  styleUrls: ['./post-page.component.css']
})
export class PostPageComponent implements OnInit {
  author;
  isSameUser = false;
  post;
  keywords;

  constructor(private http: FirebaseService, private route: ActivatedRoute, private messageService: CategoryListService) { 
  }

  showSpinner: boolean = true;
  ngOnInit() {
    if (this.http.user) this.author = this.http.user.email;
    window.scroll(0, 0);
    let id = '';
    let route = '';
    this.route.params.subscribe(params => {
      id = params.id;
      route = params.route;
      
      this.http.getSeparatePost(id).subscribe(data => {
        this.showSpinner = false;
        this.post = data;
        
        (this.post.keywords) ? this.post.keywords = this.post.keywords.toString().split(', ') : this.post.keywords = null;
        if (this.post.keywords) this.keywords = this.post.keywords.join(", ");
        if (this.post) if (this.author == this.post.author) this.isSameUser = true;
        
      });
    });
  }

  delete() {
    this.http.deleteData(this.post)
    location.reload();
  }

  photoError(event) {
    event.target.src = this.http.imgErr;
  }

}
