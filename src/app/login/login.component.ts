import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { FirebaseService } from '../firebase.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  constructor(private auth: FirebaseService, private router: Router) { this.loginGroup = this.LoginForm(); }

  title = 'Sign in';
  submitted = false;
  toggleText = 'Register';
  isLogin = true;
  fireErr;

  loginGroup: FormGroup;
  LoginForm() {
    return new FormGroup({
      email: new FormControl('', [Validators.required]),
      password: new FormControl('', [Validators.required])
    });
  }
  get f() { return this.loginGroup.controls; }

  onSubmit(v) {
    if (this.loginGroup.invalid) {
      this.submitted = true;
      return;
    }
    if (this.isLogin) {
      this.auth.login(v.email, v.password)
        .then(value => {
          this.fireErr = value;
          if (!value) {
            this.router.navigate(['']);
            location.reload();
          }
        });
    } else {
      this.auth.signup(v.email, v.password)
        .then(value => {
          this.fireErr = value;
          if (!value) this.toggler();
        });
    }


  }
  toggler() {
    this.isLogin = !this.isLogin;
    if (this.isLogin) {
      this.title = 'Sign in';
      this.toggleText = 'Register';
    } else {
      this.title = 'Register';
      this.toggleText = 'Sign in';
    }
  }

  ngOnInit() {
  }

}
