import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PostsComponent } from './posts/posts.component';
import { PostPageComponent } from './post-page/post-page.component';
import { LoginComponent } from './login/login.component';
import { UserPageComponent } from './user-page/user-page.component';

const routes: Routes = [
  { path: '',   redirectTo: '/ads/news', pathMatch: 'full' },
  { path: 'ads',   redirectTo: '/ads/news', pathMatch: 'full' },
  { path: 'ads/:route/:id', component: PostPageComponent },
  { path: 'ads/:route', component: PostsComponent },
  { path: 'authorization', component: LoginComponent },
  { path: 'userpage', component: UserPageComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
export const routingComponents = [PostsComponent];
