import { Injectable } from '@angular/core';

import { AngularFireAuth } from 'angularfire2/auth';
import * as firebase from 'firebase/app';

import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  admin = 'admin@admin.admin';
  isAdmin = false;

  user: Observable<firebase.User>;
  constructor(private firebaseAuth: AngularFireAuth) {
    this.user = firebaseAuth.authState;
    this.user.subscribe(res => {
      if (res)
        if (res.email == this.admin) {
          this.isAdmin = true;
        }
    })
  }
  signup(email: string, password: string) {
    return this.firebaseAuth
      .auth
      .createUserWithEmailAndPassword(email, password)
      .then(value => {
        console.log("success");
        return false;
      })
      .catch(err => {
        return err.message;
      });
  }

  login(email: string, password: string) {
    return this.firebaseAuth
      .auth
      .signInWithEmailAndPassword(email, password)
      .then(value => {

        return false;
      })
      .catch(err => {
        return err.message;
      });
  }
  logout() {
    this.firebaseAuth
      .auth
      .signOut();
  }
}
