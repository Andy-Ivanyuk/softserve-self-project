const express = require('express');
const app = express();
const postsRoutes = express.Router();

let Post = require('../models/posts')


postsRoutes.route('/get/:amount').get((req, res) => {
    let query = req.query;
    if (req.query._id) query._id = { "$lt": req.query._id };
    if (req.query.keywords) query.keywords = { $regex: req.query.keywords, $options: 'i' };
    if (req.query.author) query.author = { $regex: req.query.author, $options: 'i' };
    if (req.query.status) {
        query.status = req.query.status.split(',');
        query.status = { "$in": query.status };
    }

    console.log(query);

    Post.find(query).sort({ "_id": -1 }).limit(+req.params.amount).exec((err, posts) => {
        err ? console.log(err) : res.json(posts);
    })

})

postsRoutes.route('/sep/:id').get((req, res) => {
    Post.findById({ _id: req.params.id }, (err, post) => {
        err ? console.log(err) : res.json(post);
    })
})

postsRoutes.route('/add').post((req, res) => {
    let post = new Post(req.body);
    post.save()
        .then(post => {
            res.status(200).json({ 'post': 'post is added!' });
        })
        .catch(err => {
            res.status(400).send("unable to save to database");
        })
})

postsRoutes.route('/update/:id').post((req, res) => {
    Post.findOneAndReplace({ _id: req.params.id }, { $set: req.body })
        .then(post => {
            res.status(200).json({ 'post': 'post is updated!' });
        })
        .catch(err => {
            res.status(400).send("unable to update!");
        })
})

postsRoutes.route('/delete').post((req, res) => {
    let post = req.body;
    Post.updateOne({ _id: post._id }, { $set: { status: 'deleted' } })
        .then(post => {
            res.status(200).json({ 'post': 'post is updated!' });
        })
        .catch(err => {
            res.status(400).send("unable to update!");
        })
})

module.exports = postsRoutes;