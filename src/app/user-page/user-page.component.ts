import { Component, OnInit } from '@angular/core';
import { FirebaseService } from '../firebase.service';
import { HttpBackend } from '@angular/common/http';

@Component({
  selector: 'app-user-page',
  templateUrl: './user-page.component.html',
  styleUrls: ['./user-page.component.css']
})
export class UserPageComponent implements OnInit {

  constructor(private http: FirebaseService) { }
  postsList;
  showSpinner: boolean = true;
  authorizedUser;
  key;
  status = 'draft,upload';
  // selectedStatus = 'All';

  ngOnInit() {
    if (this.http.user)this.authorizedUser = this.http.user.email;
    this.loadPosts()
  }

  loadPosts() {
    this.showSpinner = true;
    if (this.http.user)
    this.http.getData({author: this.authorizedUser, status: this.status, amount: 10, id: this.key})
      .subscribe(post => {
        console.log(post);
        if (this.postsList) {

          for (const i in post) {
            this.postsList.push(post[i]);
          }
        }
        else {
          this.postsList = post;
        }
        if (this.postsList.length != 0) {
          this.key = this.postsList[this.postsList.length - 1]._id;
        }
        this.showSpinner = false;
      });
  }

  changeStatus(status) {
    this.status = status;
    this.key = undefined;
    this.postsList = null;
    this.loadPosts();
  }
  photoError(event) {
    event.target.src = this.http.imgErr;

  }

  onScroll() {
    this.loadPosts()
  }
}
