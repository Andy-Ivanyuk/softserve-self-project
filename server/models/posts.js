const mongoose = require('mongoose');

const PostSchema = new mongoose.Schema({
    type: String,
    title: String,
    category: String,
    date: Date,
    image: String,
    text: String,
    author: String,
    status: String,
    keywords: String
},{
    collection: 'posts'
})

const Post = mongoose.model('Post', PostSchema)

module.exports = Post