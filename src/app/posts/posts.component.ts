import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { FirebaseService } from '../firebase.service';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs';
import { CategoryListService } from '../category-list.service';

@Component({
  selector: 'app-posts',
  templateUrl: './posts.component.html',
  styleUrls: ['../filterPanel.css']
})
export class PostsComponent implements OnInit {

  constructor(private http: FirebaseService, private route: ActivatedRoute) {
  }

  selectedCategory = "All";

  postsList;
  title;
  categories;
  categoriesAmount = 5;
  slicedCategories;

  key;
  keywords;
  author;
  status = 'draft,upload,deleted';
  authorizedUser;

  showSpinner: boolean = true;

  @ViewChild('authorInput') authorInput: ElementRef;
  ngOnInit() {

    if (this.http.user) this.authorizedUser = this.http.user.email;

    this.route.params.subscribe(params => {
      this.key = undefined;
      this.postsList = null;
      this.author = undefined;
      this.title = params.route;
      this.loadPosts();
      this.getCategories();
      this.selectedCategory = "All";
    });

  }
  changeStatus(status) {
    this.status = status;
    this.key = undefined;
    this.postsList = null;
    this.loadPosts();
  }

  onKeywords(event) {
    if (event.length == 0) this.keywords = undefined;
    this.keywords = event;
    this.key = undefined;
    this.postsList = null;
    this.loadPosts();
  }

  onAuthor(event) {
    if (event.length == 0) this.author = undefined;
    this.author = event;
    this.key = undefined;
    this.postsList = null;
    this.loadPosts();
  }

  loadPosts() {
    this.showSpinner = true;
    this.http.getData({ type: this.title, id: this.key, category: (this.selectedCategory == 'All') ? undefined : this.selectedCategory, 
                        status: (this.http.isAdmin)? this.status : undefined ,keywords: this.keywords, author: this.author })
      .subscribe(post => {
        console.log(post);
        if (this.postsList) {

          for (const i in post) {
            this.postsList.push(post[i]);
          }
        }
        else {
        
          this.postsList = post;
        }
        if (this.postsList.length != 0) {
          this.key = this.postsList[this.postsList.length - 1]._id;
        }
        this.showSpinner = false;
      });
  }

  onScroll() {
    this.loadPosts();
  }

  getCategories() {
    let catString = "All, ";

    this.http.getCategories()
      .subscribe(categories => {
        switch (this.title) {
          case 'help':
            this.categories = catString.concat(categories[0].help).split(", ");
            break;
          case 'news':
            this.categories = catString.concat(categories[0].news).split(", ");
            break;
          default:
            this.categories = catString.concat(categories[0].posts).split(", ");   
        }
      });
  }

  setCategory(cat) {
    this.selectedCategory = cat;
    this.key = undefined;
    this.postsList = null;
    this.loadPosts();
  }

}
