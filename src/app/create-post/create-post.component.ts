import { Component, OnInit, Input } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { FirebaseService } from '../firebase.service';
import { Model } from '../post-model';

@Component({
  selector: 'app-create-post',
  templateUrl: './create-post.component.html',
  styleUrls: ['./create-post.component.css']
})
export class CreatePostComponent implements OnInit {

  @Input() modalType: string;
  @Input() prevPost?: Model;
  @Input() keywords: string;

  createPostGroup: FormGroup;

  typeList = ['News', 'Help', 'Posts'];
  temporaryList = [];

  today;
  newsCategories;
  helpCategories;
  postsCategories;

  submitted = false;
  constructor(private http: FirebaseService) {
    this.createPostGroup = this.postGroupForm();
    this.today = new Date().toLocaleString();
  }

  get f() { return this.createPostGroup.controls; }

  changeType(type: string) {
    if (type == "News") {
      return this.newsCategories;
    } else if (type == "Posts") {
      return this.postsCategories;
    } else {
      return this.helpCategories;
    }
  }

  postGroupForm() {
    return new FormGroup({
      type: new FormControl('', [Validators.required]),
      category: new FormControl('', [Validators.required]),
      image: new FormControl('', [Validators.required]),
      title: new FormControl('', [Validators.required]),
      text: new FormControl('', [Validators.required]),
      keywords: new FormControl()
    });
  }

  ngOnInit() {
    if (this.http.user)this.author = this.http.user.email;    
    this.createPostGroup.get("type").valueChanges.subscribe(value => {
      this.temporaryList = this.changeType(value);
    })
    this.http.getCategories()
      .subscribe(categories => {
        this.helpCategories = categories[0].help.split(", ");
        this.newsCategories = categories[0].news.split(", ");
        this.postsCategories = categories[0].posts.split(", ");
      });
  }
  author;
  // onSubmit(v) {

  //   if (this.createPostGroup.invalid) {
  //     this.submitted = true;
  //     return;
  //   }
  //   if (v.keywords) {
  //     v.keywords = v.keywords.toLowerCase();
  //   }
  //   v.date = this.date;
  //   v.author = this.author;
  //   switch (v.type) {
  //     case "News":
  //       this.firebaseService.addData(v, 'news');
  //       break;
  //     case "Posts":
  //       this.firebaseService.addData(v, 'posts');
  //       break;
  //     case "Help":
  //       this.firebaseService.addData(v, 'help');
  //       break;
  //   }
  //   this.createPostGroup.reset();
  //   document.getElementById('close').click();
  // }
  onSubmit(v, status: string = 'upload') {
    if (this.createPostGroup.invalid) {
      this.submitted = true;
      return;
    }
    v.type = v.type.toLowerCase();
    if (v.keywords) {
      v.keywords = v.keywords.toLowerCase();
    }
    v.author = this.author;
    v.status = status;
    if (this.modalType === "Create") {
      v.date = this.today;
      this.http.addData(v);
    } else {
      v.date = this.prevPost.date;
      this.http.updateData(this.prevPost._id, v);
    }
    this.createPostGroup.reset();
    document.getElementById('close').click();
    location.reload();

  }

}
