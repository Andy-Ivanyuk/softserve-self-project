import { Injectable } from '@angular/core';

import { AngularFireAuth } from 'angularfire2/auth';
import { HttpClient } from '@angular/common/http'


@Injectable({
  providedIn: 'root'
}
)
export class FirebaseService {
  user;
  isAdmin = false;
  constructor(private http: HttpClient, private firebaseAuth: AngularFireAuth) {
    this.user = JSON.parse(sessionStorage.getItem('user'));
    if (this.user) {
      if (this.user.email == this.admin)
        this.isAdmin = true;
    }
  }
  admin = 'admin@admin.admin';

  imgErr = 'https://via.placeholder.com/525x350.png?text=Image not found';


  url = 'http://localhost:1234';
  
  getData(params?) {
    if (!params.amount) params.amount = 4;
    let query = `${this.url}/posts/get/${params.amount}?`;
    if (!this.isAdmin && !params.status) {
      query += `&status=upload`;
    } else if (params.status) query += `&status=${params.status}`;
    if (params.type) query += `&type=${params.type}`;
    if (params.id) query += `&_id=${params.id}`;
    if (params.category) query += `&category=${params.category}`;
    if (params.keywords) query += `&keywords=${params.keywords}`;
    if (params.author) query += `&author=${params.author}`;
    
    return this.http.get(query);
  }

  getSeparatePost(id) {
    return this.http.get(`${this.url}/posts/sep/${id}`)
  }

  addData(data) {
    return this.http.post(`${this.url}/posts/add`, data)
      .subscribe(() => console.log('Done'));
  }

  updateData(id, data) {
    return this.http.post(`${this.url}/posts/update/${id}`, data)
      .subscribe(() => console.log('Done'));
  }

  getCategories() {
    return this.http.get(`${this.url}/categories/get`)
  }

  deleteData(data) {
    return this.http.post(`${this.url}/posts/delete`, data)
      .subscribe(() => console.log('Done'));
  }


  signup(email: string, password: string) {
    return this.firebaseAuth
      .auth
      .createUserWithEmailAndPassword(email, password)
      .then(value => {
        console.log("success");
        return false;
      })
      .catch(err => {
        return err.message;
      });
  }

  login(email: string, password: string) {
    return this.firebaseAuth
      .auth
      .signInWithEmailAndPassword(email, password)
      .then(value => {
        // this.user = value.user.email;
        sessionStorage.setItem('user', JSON.stringify(value.user));
        console.log("r");

        if (this.admin == value.user.email)
          this.isAdmin = true;

        return false;
      })
      .catch(err => {
        return err.message;
      });
  }
  logout() {
    this.firebaseAuth
      .auth
      .signOut();
    sessionStorage.removeItem('user');
    location.reload();
  }
  return() {
    return sessionStorage.getItem('user');
  }
}