import { Injectable, Output, EventEmitter } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class CategoryListService {

  constructor() { }

  @Output() change: EventEmitter<string> = new EventEmitter();

  setKeyword(value) {
    
    this.change.emit(value);
  }
}
