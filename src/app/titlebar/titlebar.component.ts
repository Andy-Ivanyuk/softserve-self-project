import { Component, OnInit } from '@angular/core';
import { FirebaseService } from '../firebase.service';
import { async } from 'q';

@Component({
  selector: 'app-titlebar',
  templateUrl: './titlebar.component.html',
  styleUrls: ['./titlebar.component.css']
})
export class TitlebarComponent implements OnInit {

  constructor(private fire: FirebaseService) { }

  userEmail;
  ngOnInit() {
    if (this.fire.user) this.userEmail = this.fire.user.email;
    
  }
  logout() {
    this.userEmail = undefined;
    this.fire.logout();
  }
}
