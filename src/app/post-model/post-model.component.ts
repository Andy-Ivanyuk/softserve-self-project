import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Model } from '../post-model';
import { FirebaseService } from '../firebase.service';

@Component({
  selector: 'app-post-model',
  templateUrl: './post-model.component.html',
  styleUrls: ['./post-model.component.css']
})
export class PostModelComponent implements OnInit {

  @Input() post: Model;
  @Input() route: string;

  @Output() category = new EventEmitter<string>();
  @Output() author = new EventEmitter<string>();

  constructor(private http: FirebaseService) { }

  ngOnInit() {
    
  }

  photoError(event) {
    event.target.src = this.http.imgErr;

  }

  setCategory(category) {
    this.category.emit(category);
  }

  setAuthor(author) {
    this.author.emit(author);
  }
}
