const express = require('express');
const app = express();
const catRoutes = express.Router();

let Categories = require('../models/categories')

catRoutes.route('/get').get((req, res) => {
    Categories.find().exec((err, cat) => {
        err ? console.log(err) : res.json(cat);
    })
})

module.exports = catRoutes ;