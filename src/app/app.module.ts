import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule, routingComponents } from './app-routing.module';
import { AppComponent } from './app.component';
import { PostModelComponent } from './post-model/post-model.component';
import { InfiniteScrollModule } from 'ngx-infinite-scroll';

// FIREBASE
import { HttpClientModule } from "@angular/common/http";
import { FirebaseService } from './firebase.service';
import { AngularFireModule } from 'angularfire2';
import { AngularFireDatabaseModule } from 'angularfire2/database';
import { AngularFireAuthModule } from 'angularfire2/auth';
import { CreatePostComponent } from './create-post/create-post.component';
import { ReactiveFormsModule } from '@angular/forms';
import { TitlebarComponent } from './titlebar/titlebar.component';
import { PostPageComponent } from './post-page/post-page.component';
import { SpinnerComponent } from './spinner/spinner.component';
import { LoginComponent } from './login/login.component';
import { UserPageComponent } from './user-page/user-page.component';

export const firebaseConfig = {
  apiKey: 'AIzaSyD86k9eCaw3YxKahOOidMhkU_HtGZOy0f8',
  authDomain: 'soft-serve-self-app.firebaseapp.com',
  databaseURL: 'https://soft-serve-self-app.firebaseio.com',
  projectId: 'soft-serve-self-app',
  storageBucket: 'soft-serve-self-app.appspot.com',
  messagingSenderId: 'ZZZZZZZZZZZZ'
};

@NgModule({
  declarations: [
    AppComponent,
    routingComponents,
    PostModelComponent,
    CreatePostComponent,
    TitlebarComponent,
    PostPageComponent,
    SpinnerComponent,
    LoginComponent,
    UserPageComponent
  ],
  imports: [
    InfiniteScrollModule,
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    AngularFireDatabaseModule,
    AngularFireAuthModule,
    ReactiveFormsModule,
    AngularFireModule.initializeApp(firebaseConfig)
  ],
  providers: [FirebaseService],
  bootstrap: [AppComponent]
})
export class AppModule { }
