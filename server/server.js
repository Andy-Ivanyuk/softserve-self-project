let express = require('express')
let bodyParser = require('body-parser')
let mongoose = require('mongoose')
let app = express()
let postsRoutes = require('./routes/posts.route');
let catRoutes = require('./routes/categories.route');

mongoose.Promise = Promise;
mongoose.connect('mongodb://localhost:27017/softservedb', { useNewUrlParser: true })
    .then(() => console.log('mongoose up!'))
    .catch((err) => console.log('error! ' + err))

// let Post = require('./models/posts')


app.use(function (req, res, next) {

    // Website you wish to allow to connect
    res.setHeader('Access-Control-Allow-Origin', '*');

    // Request methods you wish to allow
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');

    // Request headers you wish to allow
    res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type');

    // Set to true if you need the website to include cookies in the requests sent
    // to the API (e.g. in case you use sessions)
    res.setHeader('Access-Control-Allow-Credentials', true);

   // res.setHeader('Content-Type', 'application/json');

    // Pass to next layer of middleware
    next();
});
app.use(bodyParser.json());
app.use('/posts', postsRoutes);
app.use('/categories', catRoutes);

app.listen(1234, () => {
    console.log('Listening on port 1234');
});